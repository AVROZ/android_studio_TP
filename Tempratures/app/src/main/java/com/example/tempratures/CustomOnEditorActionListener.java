package com.example.tempratures;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

public class CustomOnEditorActionListener implements TextView.OnEditorActionListener {

    ConvertTemperature convertTemperature;
    EditText opposed_edit_text;
    CustomOnEditorActionListener(EditText opposed_edit_text)
    {
        this.opposed_edit_text = opposed_edit_text;
        this.convertTemperature = new ConvertTemperature();
    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        int writtenValue;
        int convertedValue;

        String resultString;

        if(textView.getId() == R.id.cel_editText)
        {
            String string = String.valueOf(textView.getText());
            writtenValue = Integer.parseInt(string);


            convertedValue = this.convertTemperature.convertTemp("far", writtenValue);

            resultString = String.valueOf(convertedValue);

            this.opposed_edit_text.setText(resultString);

        }

        else if(textView.getId() == R.id.far_editText)
        {
            String string = String.valueOf(textView.getText());
            writtenValue = Integer.parseInt(string);

            convertedValue = this.convertTemperature.convertTemp("cel", writtenValue);

            resultString = String.valueOf(convertedValue);

            this.opposed_edit_text.setText(resultString);

        }

            return true;
    }
}
