package com.example.tempratures;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    EditText cel_editText = this.findViewById(R.id.cel_editText);
    EditText far_editText = this.findViewById(R.id.far_editText);

    cel_editText.setOnEditorActionListener(new CustomOnEditorActionListener(far_editText));
    far_editText.setOnEditorActionListener(new CustomOnEditorActionListener(cel_editText));


}