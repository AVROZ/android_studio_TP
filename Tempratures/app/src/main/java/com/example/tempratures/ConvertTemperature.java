package com.example.tempratures;

public class ConvertTemperature {

    public ConvertTemperature(){

    }

    public int convertTemp(String temp_type, int value)
    {
        int result = 0;

        if(temp_type.equals("far"))
        {
            result = (value - 32) * 5/9;
        }
        else if(temp_type.equals("cel"))
        {
            result = (value - (9/5)) + 32;
        }

        return result;
    }
}
