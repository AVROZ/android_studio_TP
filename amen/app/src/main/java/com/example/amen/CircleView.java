package com.example.amen;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class CircleView extends View {
    private Paint paintCircle;

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.paintCircle = new Paint();
        // PURPLE CIRCLE
        paintCircle.setColor(0xff800080);
        paintCircle.setStrokeWidth(1000);
    }

    public void setColor(int color)
    {
        paintCircle.setColor(color);
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(this.getWidth()/2,this.getHeight()/2,this.getHeight()/2, this.paintCircle);

    }

}
