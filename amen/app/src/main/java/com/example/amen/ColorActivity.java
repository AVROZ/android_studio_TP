package com.example.amen;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.widget.Button;

public class ColorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu);
        int posiListe = Integer.parseInt(getIntent().getStringExtra("positionListe"));


        Button annuler = findViewById(R.id.annuler);
        annuler.setOnClickListener(new AnnulerListener(this));

        Button supprimer = findViewById(R.id.annuler);
        supprimer.setOnClickListener(new SupprimerListener(this, posiListe));
    }

}
