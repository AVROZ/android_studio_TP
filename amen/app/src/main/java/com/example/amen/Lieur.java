package com.example.amen;

import android.database.Cursor;
import android.util.Log;
import android.view.View;
import android.widget.SimpleCursorAdapter;

public class Lieur implements SimpleCursorAdapter.ViewBinder {
    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {

        if(view.getId() == R.id.couleur) {
            Log.d("Debug", view + " / " + cursor.getInt(columnIndex));

            CircleView circleview = (CircleView)view;
            circleview.setColor(cursor.getInt(columnIndex));

            return true;
        }
        return false;
    }

}
