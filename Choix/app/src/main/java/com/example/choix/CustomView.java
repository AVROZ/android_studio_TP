package com.example.choix;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class CustomView extends View
{

    private Paint paintRect;
    private Paint paintCircle;
    private Paint paintCross;
    private Paint paintTriangle;


    private String form;

    private Resources res;




    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        /* RECTANBLE */
        this.paintRect = new Paint();



        this.paintCircle = new Paint();
        // PURPLE CIRCLE
        paintCircle.setColor(0xff800080);
        paintCircle.setStyle(Paint.Style.STROKE); //
        paintCircle.setStrokeWidth(50);


        this.paintTriangle = new Paint();
        paintCircle.setColor(0xff800080);

        this.paintCross = new Paint();
        paintCircle.setColor(0xff800080);

        this.res = this.getResources();
        this.form = "square";

    }


    public void setForm(String form)
    {

        this.form = form;

        System.out.println("Selected form : " + this.form);

        this.invalidate(); // actualise l'affichage'

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        if(form.equals("square"))
        {
            canvas.drawRect(100, 100, 200, 200, paintRect);

        }
        else if(form.equals("circle"))
        {
            canvas.drawCircle(200,200,40, paintCircle);

        }
        else if(form.equals("cross"))
        {

        }
        else if(form.equals("triangle"))
        {
            Path path = new Path();
            int halfWidth = 10;
            int x = 50;
            int y = 50;
            path.moveTo(x, y - halfWidth); // Top
            path.lineTo(x - halfWidth, y + halfWidth); // Bottom left
            path.lineTo(x + halfWidth, y + halfWidth); // Bottom right
            path.lineTo(x, y - halfWidth); // Back to Top
            path.close();

            canvas.drawPath(path, paintTriangle);
        }



    }





}
