package com.example.choix;

import android.widget.RadioGroup;

public class CustomRadioController implements RadioGroup.OnCheckedChangeListener {

    private CustomView resultView;

    public CustomRadioController(CustomView resultView)
    {
        this.resultView = resultView;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {

        if(i == R.id.cross_button)
        {
            this.resultView.setForm("cross");
        }
        else if(i == R.id.square_button)
        {
            this.resultView.setForm("square");

        }
        else if(i == R.id.circle_button)
        {
            this.resultView.setForm("circle");

        }
        else if(i ==  R.id.triangle_button)
        {
            this.resultView.setForm("triangle");

        }

    }
}
