package com.example.choix;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RadioGroup radioGroup = this.findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(
                new CustomRadioController(this.findViewById(R.id.result_image))
        );


    }
}