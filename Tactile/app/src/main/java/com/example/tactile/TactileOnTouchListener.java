package com.example.tactile;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class TactileOnTouchListener implements View.OnTouchListener {

    TactileCircleView tactileView;

    public TactileOnTouchListener(TactileCircleView tactileView)
    {
        this.tactileView = tactileView;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
        {
            tactileView.addCircle(0);
            tactileView.setPosView((int)motionEvent.getX(0), (int)motionEvent.getY(0), 0);
        }

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
        {
            tactileView.removeCircle(0);
        }


        if(motionEvent.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN)
        {
            tactileView.addCircle(motionEvent.getActionIndex());
            tactileView.setPosView((int)motionEvent.getX(0), (int)motionEvent.getY(0), motionEvent.getActionIndex());
        }

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_POINTER_UP)
        {
            tactileView.removeCircle(motionEvent.getActionIndex());
        }



        if(motionEvent.getActionMasked() == MotionEvent.ACTION_MOVE) {

            for(int i = 0; i< motionEvent.getPointerCount(); i++) {
                tactileView.setPosView((int) motionEvent.getX(i), (int) motionEvent.getY(i), i);
            }
        }


        return true;
    }
}
