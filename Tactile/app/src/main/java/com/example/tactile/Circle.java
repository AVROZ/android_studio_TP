package com.example.tactile;

import android.graphics.Color;

import java.util.Random;

public class Circle {

    public int x;
    public int y;
    public int[] rgb;

    public Circle()
    {
        Random rand = new Random();
        int r = rand.nextInt();
        int g = rand.nextInt();
        int b = rand.nextInt();


        Random rnd = new Random();
        this.rgb = new int[3];

        for(int i = 0; i<this.rgb.length; i++)
        {
            this.rgb[i] = rnd.nextInt(256);
        }

        this.x = 0;
        this.y = 0;
    }
    
    public int[] getColor()
    {
        return this.rgb;
    }

    public void setPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getXPos()
    {
        return this.x;
    }


        public int getYPos()
        {
            return this.y;
        }

    public boolean exist()
    {
        if(this.x == -1 && this.y == -1)
        {
            return false;
        }
        return true;
    }
}
