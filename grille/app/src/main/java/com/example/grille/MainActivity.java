package com.example.grille;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        GridLayout gridLayout = this.findViewById(R.id.grid_id);
        int j = 0;
        View[][] viewsColor = new View[3][3];
        for(int i = 0; i < gridLayout.getChildCount(); i++) {
            viewsColor[i][j] = gridLayout.getChildAt(i);
            viewsColor[i][j].setOnTouchListener(new TouchListener());

            viewsColor[i][j].setBackgroundColor(0xff800080);
            viewsColor[i][j].invalidate();
            if(i%3 == 0)
            {
                j++;
            }
        }


    }
}