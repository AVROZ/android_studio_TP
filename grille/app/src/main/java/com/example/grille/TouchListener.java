package com.example.grille;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class TouchListener implements View.OnTouchListener {
    public TouchListener()
    {

    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        view.setBackgroundColor(0xffFF5F1F);
        return true;
    }
}
