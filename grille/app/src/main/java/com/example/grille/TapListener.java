package com.example.grille;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

public class TapListener implements GestureDetector.OnDoubleTapListener {

    View view;

    public TapListener(View view)
    {
        this.view = view;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        this.view.setBackgroundColor(0xffFF5F1F);
        this.view.invalidate();
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent motionEvent) {
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return true;
    }
}
