package com.example.tp3;

import android.view.View;


public class ButtonListener implements View.OnClickListener {

    private ArrowView arrowView;

    private String pos;

    public ButtonListener(ArrowView arr, String pos)
    {
        this.arrowView = arr;
        this.pos = pos;
    }

    @Override
    public void onClick(View view) {
        if(this.pos == "left")
        {
            this.arrowView.switchImage(1);
        }
        else
        {
            this.arrowView.switchImage(0);

        }
    }
}
