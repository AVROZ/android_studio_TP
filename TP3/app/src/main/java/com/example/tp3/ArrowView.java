package com.example.tp3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

@SuppressLint("AppCompatCustomView")
public class ArrowView extends ImageView
{

    private Paint paint;
    private Resources res;



    public ArrowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        //this.paint = new Paint();
       // this.res = this.getResources();


    }

    public void switchImage(int position)
    {
        System.out.println("imageSwitched");


        if(position == 1) // LEFT ARROW
        {
            this.setImageResource(R.drawable.fleche_gauche);

        } else
        {
            this.setImageResource(R.drawable.fleche_droite);

        }

        this.invalidate(); // actualise l'affichage
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Image

       // canvas.drawBitmap(R.drawable.fleche_droite, 10, 299, paint);


    }





}
