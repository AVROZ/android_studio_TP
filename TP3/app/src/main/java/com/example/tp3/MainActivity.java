package com.example.tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button leftButton = this.findViewById(R.id.buttonleft);
        Button rightButton = this.findViewById(R.id.buttonright);

        ArrowView arrowImage = this.findViewById(R.id.imageView);

        ButtonListener listener = new ButtonListener(arrowImage, "left");
        ButtonListener listener2 = new ButtonListener(arrowImage, "right");

        leftButton.setOnClickListener(listener);
        rightButton.setOnClickListener(listener2);


    }
}