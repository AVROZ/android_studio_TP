package com.example.arcenciel;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class BackgroundTactile extends View {

    ArrayList<Integer> colorList;
    int currentColor;

    public BackgroundTactile(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.colorList = new ArrayList<Integer>();
        this.currentColor = Color.WHITE;

        this.setOnTouchListener(new TactileTouchListener(this));
    }

    public int getCurrentColor()
    {
        return this.currentColor;
    }

    public void interNextColor()
    {
        int index = (int)(Math.random() * colorList.size());
        this.currentColor = colorList.get(index);
    }

    public void addColor(int color)
    {
        colorList.add(color);
    }
    public void removeColor(int color)
    {
        colorList.remove(color);

    }

    public ArrayList<Integer> getColorList()
    {
        return colorList;
    }

    public void setColorList(ArrayList<Integer> colorList)
    {
        this.colorList = colorList;
    }
}
