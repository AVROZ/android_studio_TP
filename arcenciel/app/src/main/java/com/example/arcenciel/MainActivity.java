package com.example.arcenciel;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button buttonSelect = this.findViewById(R.id.button_select);
        BackgroundTactile backgroundTactile = this.findViewById(R.id.background_tactile);


        Bundle bundleFromPreviousActivity = getIntent().getExtras();

        //System.out.println(bundleFromPreviousActivity.getString("colors", "null"));
        //ArrayList<Integer> arrayList = ArrayListSerializer.parse(bundleFromPreviousActivity.getString("colors", "null"));

        //backgroundTactile.setColorList(arrayList);

        buttonSelect.setOnClickListener(new ButtonListener(this, backgroundTactile));

    }
}