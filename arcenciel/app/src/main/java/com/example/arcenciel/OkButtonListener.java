package com.example.arcenciel;

import android.content.Intent;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class OkButtonListener implements View.OnClickListener {

    private AppCompatActivity selectColorActivity;
    ArrayList<Integer> colorList;
    public OkButtonListener(AppCompatActivity ac, ArrayList<Integer> colorList)
    {
        this.colorList = colorList;
    this.selectColorActivity = ac;
    }

    @Override
    public void onClick(View view) {

        Intent i = new Intent(this.selectColorActivity, MainActivity.class);

        i.putExtra("colors", ArrayListSerializer.serialize(colorList));

        this.selectColorActivity.startActivity(i);

    }
}
