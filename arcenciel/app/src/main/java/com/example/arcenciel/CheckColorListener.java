package com.example.arcenciel;

import android.graphics.Color;
import android.widget.CompoundButton;

import java.util.ArrayList;

public class CheckColorListener implements CompoundButton.OnCheckedChangeListener {

    ArrayList<Integer> colorList;
    int selectedColor;

    public CheckColorListener(ArrayList<Integer> colorList, int color)
    {
        this.colorList = colorList;
        this.selectedColor = color;
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(!b)
            this.colorList.remove(this.selectedColor);

        if(b)
            this.colorList.add(this.selectedColor);
    }
}
