package com.example.arcenciel;

import android.content.Intent;
import android.view.View;

import java.util.ArrayList;

public class ButtonListener implements View.OnClickListener {

    private MainActivity mainActivity;
    BackgroundTactile backgroundTactile;

    public ButtonListener(MainActivity mainActivity, BackgroundTactile backgroundTactile)
    {
        this.mainActivity = mainActivity;
        this.backgroundTactile = backgroundTactile;
    }

    @Override
    public void onClick(View view) {
        Intent i = new Intent(this.mainActivity, ActivityTwo.class);
        ArrayList<Integer> colorList = backgroundTactile.getColorList();

        i.putExtra("colors", ArrayListSerializer.serialize(colorList));

        this.mainActivity.startActivity(i);

    }
}
