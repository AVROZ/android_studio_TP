package com.example.arcenciel;

import java.util.ArrayList;

public class ArrayListSerializer {


    public static String serialize(ArrayList<Integer> arrayList)
    {
        String result = "";

        for (int integer : arrayList) {
            result += integer + ',';

        }

        return result;
    }

    public static ArrayList<Integer> parse(String stringList)
    {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        String[] words = stringList.split(",");


        for (String strNum : words) {
            arrayList.add(Integer.parseInt(strNum));
        }

        return arrayList;



    }


}
