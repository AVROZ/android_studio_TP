package com.example.arcenciel;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class TactileTouchListener implements View.OnTouchListener {

    BackgroundTactile backgroundTactile;
    public TactileTouchListener(BackgroundTactile backgroundTactile)
    {
    this.backgroundTactile = backgroundTactile;
    }


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {


        backgroundTactile.interNextColor();
        backgroundTactile.setBackgroundColor(backgroundTactile.getCurrentColor());
        backgroundTactile.invalidate();

        return true;
    }
}
