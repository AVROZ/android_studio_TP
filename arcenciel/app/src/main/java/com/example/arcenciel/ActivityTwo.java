package com.example.arcenciel;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ActivityTwo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        Button buttonOk = this.findViewById(R.id.ok_btn);

        Bundle bundleFromPreviousActivity = getIntent().getExtras();
        System.out.println(bundleFromPreviousActivity.getString("colors", "null"));

        ArrayList<Integer> arrayList = ArrayListSerializer.parse(bundleFromPreviousActivity.getString("colors", "null"));

        CheckBox red_CheckBox = this.findViewById(R.id.red_CheckBox);
        red_CheckBox.setOnCheckedChangeListener(new CheckColorListener(arrayList, Color.RED));

        CheckBox purple_CheckBox = this.findViewById(R.id.purple_CheckBox);
        purple_CheckBox.setOnCheckedChangeListener(new CheckColorListener(arrayList, Color.BLACK));

        CheckBox blue_CheckBox = this.findViewById(R.id.blue_CheckBox);
        blue_CheckBox.setOnCheckedChangeListener(new CheckColorListener(arrayList, Color.BLUE));

        CheckBox orange_CheckBox = this.findViewById(R.id.orange_CheckBox);
        orange_CheckBox.setOnCheckedChangeListener(new CheckColorListener(arrayList, Color.YELLOW));


        buttonOk.setOnClickListener(new OkButtonListener(this, arrayList));



    }
}
