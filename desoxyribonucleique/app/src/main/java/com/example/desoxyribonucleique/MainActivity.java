package com.example.desoxyribonucleique;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private ObserverPrefs observerPrefs; // Permet qu'il tourne tout le temps

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Préférences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false); // On prend le fichier XML des préférences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this); // Récupération des préféences

        //Log.d("debug", "Préférence actuelle : " + prefs.getString("pref_color","oups") ); // Utilisation du dictionnaire pour récupérer les données

        // Listener pour actualiser les vues tout le temps
        Button[] buttons = new Button[4];
        buttons[0] = findViewById(R.id.button_A);
        buttons[1] = findViewById(R.id.button_T);
        buttons[2] = findViewById(R.id.button_C);
        buttons[3] = findViewById(R.id.button_G);
        observerPrefs = new ObserverPrefs(buttons, findViewById(R.id.background_view), this.getResources());

        prefs.registerOnSharedPreferenceChangeListener(observerPrefs);

        // Listener pour faire l'exercice
        EditText editText = findViewById(R.id.editText);
        findViewById(R.id.button_A).setOnClickListener(new AddNucleotideListener(editText, "A"));
        findViewById(R.id.button_T).setOnClickListener(new AddNucleotideListener(editText, "T"));
        findViewById(R.id.button_C).setOnClickListener(new AddNucleotideListener(editText, "C"));
        findViewById(R.id.button_G).setOnClickListener(new AddNucleotideListener(editText, "G"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_nucleique, menu); // Gonfle le menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id_background_color_change = item.getItemId();

        if(id_background_color_change == R.id.change_color)
        {
            Log.d("debug", "Activité du menu des préférences chargée !");
            this.startActivity(new Intent(this, SettingsActivity.class)); // Activity des preferences
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}