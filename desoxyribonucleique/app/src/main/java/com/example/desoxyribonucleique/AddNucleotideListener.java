package com.example.desoxyribonucleique;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class AddNucleotideListener implements View.OnClickListener {

    private String nucleotide;
    private EditText editText;

    public AddNucleotideListener(EditText editText, String nucleotide){
        this.editText = editText;
        this.nucleotide = nucleotide;
    }


    @Override
    public void onClick(View view) {
        Log.d("debug", "Nucleotide " + this.nucleotide + " ajouté!");
        this.editText.setText(this.editText.getText() + this.nucleotide);
    }
}
