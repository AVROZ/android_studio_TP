package com.example.desoxyribonucleique;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class ObserverPrefs implements SharedPreferences.OnSharedPreferenceChangeListener {
    Button[] buttons;
    LinearLayout backgroundView;
    Resources resources;
    public ObserverPrefs(Button[] buttons, LinearLayout backgroundView, Resources ressources){
    this.buttons = buttons;
    this.backgroundView = backgroundView;
    this.resources = ressources;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) { // Correspond à la clé actuelle

        //Log.d("debug", "s = " + s);

        if(s.equals("pref_color")) // agir sur la couleur de fond
        {
            Log.d("debug", "Couleur changée : " + sharedPreferences.getString(s, "none"));

            int colorInt = Color.parseColor(sharedPreferences.getString(s, "#FFFFFFFF"));
            this.backgroundView.setBackgroundColor(colorInt);
        }

        if(s.equals( "a_nucleotide"))
        {
            Log.d("debug", "Nucleotide A : " + sharedPreferences.getBoolean(s, true));
            this.buttons[0].setEnabled(sharedPreferences.getBoolean(s, true));
        }
        if(s.equals( "t_nucleotide"))
        {
            Log.d("debug", "Nucleotide T : " + sharedPreferences.getBoolean(s, true));
            this.buttons[1].setEnabled(sharedPreferences.getBoolean(s, true));
        }
        if(s.equals( "c_nucleotide"))
        {
            Log.d("debug", "Nucleotide C : " + sharedPreferences.getBoolean(s, true));
            this.buttons[2].setEnabled(sharedPreferences.getBoolean(s, true));
        }
        if(s.equals( "g_nucleotide"))
        {
            Log.d("debug", "Nucleotide T : " + sharedPreferences.getBoolean(s, true));
            this.buttons[3].setEnabled(sharedPreferences.getBoolean(s, true));
        }

    }
}
