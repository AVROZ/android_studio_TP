package com.example.rticule;

public class Circle {

    public int x;
    public int y;

    public Circle()
    {
        this.x = 0;
        this.y = 0;
    }

    public void setPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getXPos()
    {
        return this.x;
    }


    public int getYPos()
    {
        return this.y;
    }

    public boolean exist()
    {
        if(this.x == -1 && this.y == -1)
        {
            return false;
        }
        return true;
    }
}
