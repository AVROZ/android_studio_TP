package com.example.rticule;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TactileCircleView extends View {

    private Paint paintCircle;

    private ArrayList<Circle> circles;

    public TactileCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.setOnTouchListener(new TactileOnTouchListener(this));
        this.paintCircle = new Paint();

        this.circles = new ArrayList<>();
        paintCircle.setColor(0xff800080);
        paintCircle.setStyle(Paint.Style.STROKE);
        paintCircle.setStrokeWidth(50);

        this.addCircle(0);
        this.setPosView(100, 100, 0);
    }

    public void addCircle(int index)
    {
        Circle circle = new Circle();
        this.circles.add(index, circle);

    }

    public void removeCircle(int index)
    {
        this.circles.remove(index);

    }

    public void setPosView(int x, int y, int index)
    {
        Circle circle = this.circles.get(index);
        circle.setPos(x, y);
        this.invalidate();
    }
    public ArrayList<Circle> getCircles()
    {
        return this.circles;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(!this.circles.isEmpty()) {
            for (Circle circle : this.circles) {
                canvas.drawCircle(circle.getXPos(), circle.getYPos(), 40, paintCircle);
            }
        }

    }


}
