package com.example.rticule;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

public class TactileOnTouchListener implements View.OnTouchListener {

    TactileCircleView tactileView;

    public TactileOnTouchListener(TactileCircleView tactileView)
    {
        this.tactileView = tactileView;

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN)
        {
            //tactileView.addCircle(0);
            //tactileView.setPosView((int)motionEvent.getX(0), (int)motionEvent.getY(0), 0);
            //System.out.println("BEGIN");
        }

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_UP)
        {
            //System.out.println("END");
            //tactileView.removeCircle(0);
        }


        if(motionEvent.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN)
        {
            /*
            tactileView.addCircle(motionEvent.getActionIndex());
            tactileView.setPosView((int)motionEvent.getX(0), (int)motionEvent.getY(0), motionEvent.getActionIndex());
            System.out.println("POINTER +");
            */
        }

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_POINTER_UP)
        {
            /*
            tactileView.removeCircle(motionEvent.getActionIndex());
            System.out.println("POINTER -");
            */

        }

        if(motionEvent.getActionMasked() == MotionEvent.ACTION_MOVE)
        {
            int i;
            for(i = 0; i< motionEvent.getPointerCount(); i++)
            {
                tactileView.setPosView((int) motionEvent.getX(0), (int) motionEvent.getY(0), i);
            }
        }

        return true;
    }
}
