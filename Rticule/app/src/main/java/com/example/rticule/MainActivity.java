package com.example.rticule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("onCreate()");


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        System.out.println("OnSaveInstance");

        TactileCircleView view = this.findViewById(R.id.tactile_view);
        ArrayList<Circle> circles = view.getCircles();
        Circle circle = circles.get(0);

        savedInstanceState.putInt("x", circle.getXPos());
        savedInstanceState.putInt("y", circle.getYPos());

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        System.out.println("onRestoreInstanceState");

        System.out.println(savedInstanceState.getInt("x", 100));
        System.out.println(savedInstanceState.getInt("y", 100));


        TactileCircleView view = this.findViewById(R.id.tactile_view);
        ArrayList<Circle> circles = view.getCircles();
        Circle circle = circles.get(0);

        circle.setPos(savedInstanceState.getInt("x", 100), savedInstanceState.getInt("y", 100));
    }
}