package fr.iut_fbleau.apl41.couleurs;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public class AnnulerListener implements View.OnClickListener {
    private Activity parent;

    public AnnulerListener(Activity a){
        this.parent = a;
    }

    @Override
    public void onClick(View view) {
        this.parent.startActivity(new Intent(this.parent, Liste.class));
    }
}
