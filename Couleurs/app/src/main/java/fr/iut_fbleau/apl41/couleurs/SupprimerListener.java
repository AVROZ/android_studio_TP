package fr.iut_fbleau.apl41.couleurs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;

public class SupprimerListener implements View.OnClickListener {
    private Activity parent;
    int posiListe;
    private String[] CHAMPS = {AccesBaseDeDonnees.CHAMP_NOM, AccesBaseDeDonnees.CHAMP_VALEUR, BaseColumns._ID};


    public SupprimerListener(Activity a, int positionListe){
        this.parent = a;
        this.posiListe = positionListe;
    }

    @Override
    public void onClick(View view) {

        //recuperation base de donnees
        SQLiteDatabase db = Liste.acces.getWritableDatabase();

        Cursor curseur = db.query(AccesBaseDeDonnees.NOM_TABLE,  CHAMPS,  null, null,null,null,null,null);
        int champValeurIndex = curseur.getColumnIndex(AccesBaseDeDonnees.CHAMP_VALEUR);
        int champNomIndex = curseur.getColumnIndex(AccesBaseDeDonnees.CHAMP_NOM);
        int champIDIndex = curseur.getColumnIndex(BaseColumns._ID);
        Log.d("Debug", champValeurIndex +  "|" +champNomIndex +"|" +champIDIndex );

        while(curseur.moveToNext()) {
            if (curseur.getPosition() == posiListe) {
                Log.d("Debug", curseur.getColumnCount() + "");
                Log.d("Debug", champValeurIndex + "");
                Log.d("Debug", posiListe + "");
                Log.d("Debug", curseur.getInt(champValeurIndex) + "");
                Log.d("Debug", curseur.getString(champNomIndex));
                db.delete(AccesBaseDeDonnees.NOM_TABLE, BaseColumns._ID + "=" + curseur.getInt(champIDIndex), null);
            }
        }

        this.parent.startActivity(new Intent(this.parent, Liste.class));
    }
}
